<%-- 
    Document   : index
    Created on : 05-05-2021, 22:29:58
    Author     : JuanArriagada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
 
        <form name="fmlogin" action="">
            <table >
                <h1>Ingreso Datos Alumno</h1><!-- Titulo de la tabla-->
                <tr >
                    <td width="150"><b>Ingreso Nombre</b></td><!--Celda de la etiqueta nombre-->
                    <td><input name="txtNombre" type="text"></td>
                </tr>
                <tr>
                    <td height="25">&nbsp;</td>
                </tr>
                <tr>
                    <td width="150"><b>Indicar Sección</b></td><!--Celda de la etiqueta Sección-->
                    <td><input name="txtSecccion" type="text"></td>
                </tr>
                <tr>
                    <td height="25" colspan="2">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td><input type="submit" value="Aceptar"></td>
                    <td><input type="submit" value="Cancelar"></td>
                </tr>
            </table> 
        </form>    
        
    </body>
</html>
